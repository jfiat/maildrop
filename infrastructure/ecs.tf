# ECS cluster

resource "aws_ecs_cluster" "maildrop" {
  name = "maildrop-cluster"
}

# IAM role for ECS containers

data "aws_iam_role" "ecs" {
  name = "maildrop_ecs"
}

data "aws_iam_role" "ec2" {
  name = "ecsInstanceRole"
}

output "lambda_role" {
  value = "${data.aws_iam_role.ecs.arn}"
}

# EC2 instances

resource "aws_launch_configuration" "ec2" {
  image_id = "ami-0302f3ec240b9d23c"
  instance_type = "t2.small"
  iam_instance_profile = "${data.aws_iam_role.ec2.name}"
  key_name = "${var.key_name}"
  associate_public_ip_address = true
  security_groups = ["${aws_security_group.ec2.id}"]
  lifecycle {
    create_before_destroy = true
  }
  root_block_device {
    volume_type = "gp2"
    volume_size = 50
  }
  user_data = <<EOF
#!/bin/bash
echo ECS_CLUSTER=${aws_ecs_cluster.maildrop.name} >> /etc/ecs/ecs.config
service postfix stop
EOF
}

# EC2 autoscaling group

resource "aws_autoscaling_group" "asg" {
  max_size = 2
  min_size = 1
  desired_capacity = 1
  launch_configuration = "${aws_launch_configuration.ec2.name}"
  vpc_zone_identifier = ["${aws_subnet.public.id}"]
  availability_zones = ["${data.aws_availability_zones.available.names[0]}"]
  default_cooldown = 300
  health_check_grace_period = 300
  health_check_type = "EC2"
  tag {
    key = "Name"
    propagate_at_launch = true
    value = "maildrop-ecs"
  }
}

resource "aws_autoscaling_policy" "asp" {
  autoscaling_group_name = "${aws_autoscaling_group.asg.name}"
  name = "ec2scale"
  policy_type = "TargetTrackingScaling"
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 65.0
  }
}

# ECS task definition

resource "aws_ecs_task_definition" "taskdef" {
  family = "smtptask"
  cpu = 256
  memory = 512
  task_role_arn = "${data.aws_iam_role.ecs.arn}"
  depends_on = ["aws_elasticache_cluster.redis"]
  requires_compatibilities = ["EC2"]
  # network_mode = "awsvpc"
  execution_role_arn = "${data.aws_iam_role.ecs.arn}"
  container_definitions = <<EOF
[
  {
    "cpu": 256,
    "image": "${var.app_image}",
    "memory": 512,
    "name": "smtpapp",
    "networkMode": "bridge",
    "portMappings": [
      {
        "containerPort": 25
      }
    ],
    "essential": true,
    "environment": [
      { "name": "REDIS_HOST", "value": "${aws_elasticache_cluster.redis.cache_nodes.0.address}" },
      { "name": "REDIS_PORT", "value": "${aws_elasticache_cluster.redis.port}" },
      { "name": "LOG_LEVEL", "value": "warn" },
      { "name": "ALTINBOX_MOD", "value": "${var.altinbox_mod}" }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${aws_cloudwatch_log_group.logs.name}",
        "awslogs-region": "us-west-2",
        "awslogs-stream-prefix": "ecs"
      }
    }
  }
]
EOF
}

# ECS service

resource "aws_ecs_service" "service" {
  name = "maildrop-service"
  cluster = "${aws_ecs_cluster.maildrop.id}"
  task_definition = "${aws_ecs_task_definition.taskdef.arn}"
  desired_count = 2
  launch_type = "EC2"

  load_balancer {
    container_name = "smtpapp"
    container_port = 25
    target_group_arn = "${aws_lb_target_group.backend.arn}"
  }

  depends_on = ["aws_lb_listener.frontend", "aws_ecs_task_definition.taskdef"]
  lifecycle {
    ignore_changes = ["desired_count", "task_definition"]
  }
}

# Auto-scaling

resource "aws_appautoscaling_target" "ecs_target" {
  max_capacity       = 5
  min_capacity       = 2
  resource_id        = "service/${aws_ecs_cluster.maildrop.name}/${aws_ecs_service.service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

# Automatically scale capacity up by one

resource "aws_appautoscaling_policy" "up" {
  name               = "cb_scale_up"
  service_namespace  = "ecs"
  resource_id        = "service/${aws_ecs_cluster.maildrop.name}/${aws_ecs_service.service.name}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 180
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }

  depends_on = ["aws_appautoscaling_target.ecs_target"]
}

# Automatically scale capacity down by one

resource "aws_appautoscaling_policy" "down" {
  name               = "cb_scale_down"
  service_namespace  = "ecs"
  resource_id        = "service/${aws_ecs_cluster.maildrop.name}/${aws_ecs_service.service.name}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 180
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }

  depends_on = ["aws_appautoscaling_target.ecs_target"]
}

# Cloudwatch alarm that triggers the autoscaling up policy

resource "aws_cloudwatch_metric_alarm" "service_cpu_high" {
  alarm_name          = "cb_cpu_utilization_high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = "85"

  dimensions = {
    ClusterName = "${aws_ecs_cluster.maildrop.name}"
    ServiceName = "${aws_ecs_service.service.name}"
  }

  alarm_actions = ["${aws_appautoscaling_policy.up.arn}"]
}

# Cloudwatch alarm that triggers the autoscaling down policy

resource "aws_cloudwatch_metric_alarm" "service_cpu_low" {
  alarm_name          = "cb_cpu_utilization_low"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = "20"

  dimensions = {
    ClusterName = "${aws_ecs_cluster.maildrop.name}"
    ServiceName = "${aws_ecs_service.service.name}"
  }

  alarm_actions = ["${aws_appautoscaling_policy.down.arn}"]
}
